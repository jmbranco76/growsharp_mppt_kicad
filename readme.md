# Grow# #

##Grow Sharp Maximum Power Point Tracking PCB.

This repository has the Kicad files for the MPPT module.

This board is the power circuit stage of the photo-voltaic panel. It read current, voltage and power of the PV panel to maximize its efficiency and and monitors the battery status. The code for the efm32ZG is [here](https://bitbucket.org/jmbranco76/growsharp_mppt_code).